package student;

public interface StudentDAO {

	public Student retrieveStudent(Integer studentId);
}
