package hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 * Created by edenayrolles on 16/10/2015.
 */
public class TP {
    public static void main(String[] args) {

        Configuration configuration = new Configuration().configure();
        configuration.addResource("hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction;

        //Creation de notre objet
        LigneCommande ligneCommande = new LigneCommande();
        ligneCommande.setQuatite(1);
        ligneCommande.setLabelProduit("Test");

        //Creation de notre objet
        LigneCommande ligneCommande2 = new LigneCommande();
        ligneCommande2.setQuatite(2);
        ligneCommande2.setLabelProduit("Test2");

        //Debut transaction
        transaction = session.beginTransaction();
        //On persist l'objet
        session.save(ligneCommande);
        session.save(ligneCommande2);
        //On valide la transaction
        transaction.commit();

        //L'id de l'objet est mis a jour automatiquement
        System.out.println(ligneCommande.getId());
        System.out.println(ligneCommande2.getId());








    }
}
