package hibernate;

/**
 * Created by edenayrolles on 16/10/2015.
 */
public class LigneCommande {

    Integer id;

    Integer quatite;

    String labelProduit;

    public LigneCommande() {
    }

    public LigneCommande(Integer id, String labelProduit, Integer quatite) {
        this.id = id;
        this.labelProduit = labelProduit;
        this.quatite = quatite;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuatite() {
        return quatite;
    }

    public void setQuatite(Integer quatite) {
        this.quatite = quatite;
    }

    public String getLabelProduit() {
        return labelProduit;
    }

    public void setLabelProduit(String labelProduit) {
        this.labelProduit = labelProduit;
    }
}
