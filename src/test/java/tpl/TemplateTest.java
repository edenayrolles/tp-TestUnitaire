package tpl;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by edenayrolles on 16/10/2015.
 */
public class TemplateTest {

    private HashMap<String, String> myTab;

    @Before
    public void init() {
        myTab = new HashMap<String, String>();
        myTab.put("prenom", "Eric");
        myTab.put("nom", "Denayrolles");
    }

    @Test
    public void testRegEx() {
        String demande = "Bonjour  Eric Denayrolles";

        try {
            assertEquals(demande,  Template.evaluateRegEx("Bonjour  ${prenom} ${nom}", myTab));
        } catch (MissingValueException e) {
            fail();
        }
    }

    @Test
    public void testTpl() {
        String demande = "Bonjour  Eric Denayrolles";

        try {
            assertEquals(demande,  Template.evaluate("Bonjour  ${prenom} ${nom}", myTab));
        } catch (MissingValueException e) {
            fail();
        }
    }
}
