package student;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by edenayrolles on 16/10/2015.
 */
public class StudentServiceTest {

    private StudentServiceImpl studentService;

    private StudentDAO studentDAO;

    @Before
    public void init() {
        this.studentService = new StudentServiceImpl();
        //StudentDAO mock
        this.studentDAO = mock(StudentDAO.class);

        this.studentService.setStudentDAO(this.studentDAO);
    }

    @Test
    public void computeAverageForClassicStudent() {
        Student student = new Student();
        student.addNote(new Double(16));
        student.addNote((double)20);

        when(this.studentDAO.retrieveStudent(31))
                .thenReturn(student);



        assertEquals(new Double(18),  this.studentService.computeAverage(31));


    }

}
